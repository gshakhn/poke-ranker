package com.gshakhn.pokeranker

import java.util.concurrent.atomic.AtomicLong

import POGOProtos.Enums.PokemonFamilyIdOuterClass.PokemonFamilyId
import POGOProtos.Enums.PokemonIdOuterClass.PokemonId
import POGOProtos.Enums.PokemonMoveOuterClass.PokemonMove
import com.pokegoapi.api.pokemon.{EvolutionFormFactory, PokemonMetaRegistry}
import org.scalatest.{FunSpec, Matchers}

class PokeCategorizerSpec extends FunSpec with Matchers {
  describe("empty list of pokes") {
    val pokes = Set.empty[Poke]
    val categorizer = new PokeCategorizer(PokemonFamilyId.FAMILY_UNSET, 0)
    it("returns an empty list") {
      categorizer.categorize(pokes) shouldBe empty
    }
  }

  val uniqueId = new AtomicLong()
  def createPoke(pokemonId: PokemonId, moveSet: MoveSet, totalStats: Int): Poke = {
    Poke(uniqueId.getAndIncrement(), pokemonId, 0, 0, moveSet, totalStats / 3, totalStats / 3, totalStats / 3)
  }

  describe("for a single evolution family") {
    implicit val family = PokemonFamilyId.FAMILY_JYNX
    val perfectJynx = createPoke(PokemonId.JYNX, MoveSet(PokemonMove.POUND_FAST, PokemonMove.ICE_PUNCH), 45)
    val anotherPerfectJynx = createPoke(PokemonId.JYNX, MoveSet(PokemonMove.POUND_FAST, PokemonMove.ICE_PUNCH), 45)
    val horribleJynx = createPoke(PokemonId.JYNX, MoveSet(PokemonMove.POUND_FAST, PokemonMove.ICE_PUNCH), 0)
    val jynxWithDifferentMoves = createPoke(PokemonId.JYNX, MoveSet(PokemonMove.POUND_FAST, PokemonMove.DRAINING_KISS), 30)

    describe("with no candy") {
      implicit val candy = 0

      describe("no pokemon") {
        val pokes = Set.empty[Poke]

        it("returns an empty list") {
          categorizer.categorize(pokes) shouldBe empty
        }
      }

      describe("one pokemon") {
        val pokes = Set(perfectJynx)

        it("should keep that pokemon") {
          categorizer.categorize(pokes) shouldBe Set((perfectJynx, Keep))
        }
      }

      describe("two pokemon with same move set and same perfection") {
        val pokes = Set(perfectJynx, anotherPerfectJynx)

        it("should keep both pokemon") {
          categorizer.categorize(pokes) shouldBe Set((perfectJynx, Keep), (anotherPerfectJynx, Keep))
        }
      }

      describe("two pokemon with same move set") {
        val pokes = Set(perfectJynx, horribleJynx)

        it("should keep the better pokemon") {
          categorizer.categorize(pokes) should contain (perfectJynx, Keep)
        }

        it("should trash the worse pokemon") {
          categorizer.categorize(pokes) should contain (horribleJynx, Trash)
        }
      }

      describe("two pokemon with different move set") {
        val pokes = Set(perfectJynx, jynxWithDifferentMoves)

        it("should keep both pokemon") {
          categorizer.categorize(pokes) shouldBe Set((perfectJynx, Keep), (jynxWithDifferentMoves, Keep))
        }
      }
    }
  }

  describe("for a two evolution family") {
    implicit val family = PokemonFamilyId.FAMILY_RATTATA
    val perfectRattata = createPoke(PokemonId.RATTATA, MoveSet(PokemonMove.BITE_FAST, PokemonMove.DIG), 45)
    val averageRattata = createPoke(PokemonId.RATTATA, MoveSet(PokemonMove.BITE_FAST, PokemonMove.DIG), 30)
    val averageRattataWithDifferentMoves = createPoke(PokemonId.RATTATA, MoveSet(PokemonMove.BITE_FAST, PokemonMove.BODY_SLAM), 30)
    val perfectRaticate = createPoke(PokemonId.RATICATE, MoveSet(PokemonMove.BITE_FAST, PokemonMove.DIG), 45)

    describe("with no candy") {
      implicit val candy = 0

      describe("one 1st evolution pokemon") {
        val pokes = Set(averageRattata)

        it("should keep that pokemon") {
          categorizer.categorize(pokes) shouldBe Set((averageRattata, Keep))
        }
      }

      describe("one 2nd evolution pokemon") {
        val pokes = Set(perfectRaticate)

        it("should keep that pokemon") {
          categorizer.categorize(pokes) shouldBe Set((perfectRaticate, Keep))
        }
      }

      describe("a 1st evolution pokemon and a 2nd evolution pokemon") {
        val pokes = Set(perfectRaticate, averageRattata)

        it("should keep both pokemon") {
          categorizer.categorize(pokes) shouldBe Set((perfectRaticate, Keep), (averageRattata, Keep))
        }
      }

      describe("two 1st evolution pokemon and a 2nd evolution pokemon") {
        val pokes = Set(perfectRaticate, perfectRattata, averageRattata)

        it("should keep the 2nd evolution pokemon") {
          categorizer.categorize(pokes) should contain (perfectRaticate, Keep)
        }

        it("should keep the better 1st evolution pokemon") {
          categorizer.categorize(pokes) should contain (perfectRattata, Keep)
        }

        it("should trash the worse 1st evolution pokemon") {
          categorizer.categorize(pokes) should contain (averageRattata, Trash)
        }
      }

      describe("two 1st evolution pokemon with different moves and a 2nd evolution pokemon") {
        val pokes = Set(perfectRaticate, perfectRattata, averageRattataWithDifferentMoves)

        it("should keep the 2nd evolution pokemon") {
          categorizer.categorize(pokes) should contain (perfectRaticate, Keep)
        }

        it("should keep the better 1st evolution pokemon") {
          categorizer.categorize(pokes) should contain (perfectRattata, Keep)
        }

        it("should trash the worse 1st evolution pokemon") {
          categorizer.categorize(pokes) should contain (averageRattataWithDifferentMoves, Trash)
        }
      }

      describe("two 1st evolution pokemon with the same perfection and a 2nd evolution pokemon") {
        val pokes = Set(perfectRaticate, averageRattata, averageRattataWithDifferentMoves)

        it("should keep the 2nd evolution pokemon") {
          categorizer.categorize(pokes) should contain (perfectRaticate, Keep)
        }

        it("should keep both 1st evolution pokemon") {
          categorizer.categorize(pokes) should contain (averageRattata, Keep)
          categorizer.categorize(pokes) should contain (averageRattataWithDifferentMoves, Keep)
        }
      }
    }

    describe("with enough candy to evolve 1") {
      implicit val candy = 25

      describe("two 1st evolution pokemon and a 2nd evolution pokemon") {
        val pokes = Set(perfectRaticate, perfectRattata, averageRattata)

        it("should keep the 2nd evolution pokemon") {
          categorizer.categorize(pokes) should contain (perfectRaticate, Keep)
        }

        it("should evolve the better 1st evolution pokemon") {
          categorizer.categorize(pokes) should contain (perfectRattata, Evolve)
        }

        it("should keep the worse 1st evolution pokemon") {
          categorizer.categorize(pokes) should contain (averageRattata, Keep)
        }
      }
    }
  }


  def categorizer(implicit family: PokemonFamilyId, candy: Int): PokeCategorizer = new PokeCategorizer(family, candy)
}