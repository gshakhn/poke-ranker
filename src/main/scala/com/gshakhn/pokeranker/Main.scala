package com.gshakhn.pokeranker

import java.util.Scanner

import POGOProtos.Enums.PokemonIdOuterClass.PokemonId
import POGOProtos.Enums.PokemonMoveOuterClass.PokemonMove
import com.pokegoapi.api.PokemonGo
import com.pokegoapi.auth.GoogleUserCredentialProvider
import okhttp3.OkHttpClient

import scala.collection.JavaConverters._

/**
  * Created by gshakhnazaryan on 8/10/16.
  */
object Main extends App {
  println("Enter authentication token below. You can get it from the following URL:")
  println(GoogleUserCredentialProvider.LOGIN_URL)
  val scanner = new Scanner(System.in)
  val authCode = scanner.nextLine()

  val httpClient = new OkHttpClient()
  val provider = new GoogleUserCredentialProvider(httpClient)
  provider.login(authCode)
  val api = new PokemonGo(provider, httpClient)
  Thread.sleep(1000)
  val libraryPokemon = api.getInventories.getPokebank.getPokemons.asScala
  val libraryPokemonByFamily = libraryPokemon.groupBy(_.getPokemonFamily)
  val pokesByFamily = libraryPokemonByFamily.mapValues(_.map{ p =>
    Poke(p.getId, p.getPokemonId, p.getCp, p.getStamina, MoveSet(p.getMove1, p.getMove2),
      p.getIndividualAttack, p.getIndividualDefense, p.getIndividualStamina)
  })

  val results: Seq[(Poke, Category)] = pokesByFamily.map {
    case (family, pokes) =>
      val categorizer = new PokeCategorizer(family, api.getInventories.getCandyjar.getCandies(family))
      categorizer.categorize(pokes.toSet)
  }.toSeq.flatten
  val sortedResults = results.sortBy { case (p, _) => (p.pokemonId, p.perfection, p.cp)}

  sortedResults.foreach { case (poke, category) =>
      category match {
        case Keep => println(poke)
        case Evolve => println(fansi.Color.Green(poke.toString))
        case Trash => println(fansi.Color.Red(poke.toString))
      }
  }
  val sizeByCategory: Map[Category, Int] = sortedResults.groupBy(_._2).mapValues(_.size)

  println(s"Keep Count: ${sizeByCategory.getOrElse(Keep, 0)}")
  println(fansi.Color.Green(s"Evolve Count: ${sizeByCategory.getOrElse(Evolve, 0)}"))
  println(fansi.Color.Red(s"Trash Count: ${sizeByCategory.getOrElse(Trash, 0)}"))
}


case class MoveSet(move1: PokemonMove, move2: PokemonMove)
case class Poke(id: Long, pokemonId: PokemonId,
                cp: Int, hp: Int,
                moveSet: MoveSet,
                attack: Int, defense: Int, stamina: Int) {
  def perfection: Double = (attack + defense + stamina) / 45.0

  override def toString: String = s"$pokemonId - ${perfection.formatted("%.4f")} - $cp - $hp - ${moveSet.move1} - ${moveSet.move2}"
}
