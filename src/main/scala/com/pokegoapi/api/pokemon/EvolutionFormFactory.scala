package com.pokegoapi.api.pokemon

import POGOProtos.Enums.PokemonIdOuterClass.PokemonId

/**
  * Created by gshakhnazaryan on 8/13/16.
  */
object EvolutionFormFactory {
  def apply(pokemonId: PokemonId): EvolutionForm = new EvolutionForm(pokemonId)
}
