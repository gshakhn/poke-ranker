package com.gshakhn.pokeranker

import POGOProtos.Enums.PokemonFamilyIdOuterClass.PokemonFamilyId
import com.pokegoapi.api.pokemon.{EvolutionFormFactory, PokemonMetaRegistry}

/**
  * Created by gshakhnazaryan on 8/15/16.
  */
class PokeCategorizer(family: PokemonFamilyId, candy: Int) {
  def categorize(pokemon: Set[Poke]): Set[(Poke, Category)] = {
    val (fullyEvolved, partiallyEvolved) = pokemon.partition{ p => EvolutionFormFactory(p.pokemonId).isFullyEvolved }
    val partiallyEvolvedSorted = partiallyEvolved.toSeq.sortBy(_.perfection).reverse
    val evolve = toEvolve(partiallyEvolvedSorted, Seq.empty, candy)
    val rest = (partiallyEvolvedSorted diff evolve).toSet
    keepBestByMoveSet(fullyEvolved) ++ keepBest(rest) ++ evolve.map((_, Evolve))
  }

  private def toEvolve(optionsToEvolve: Seq[Poke], toEvolveSoFar: Seq[Poke], candyLeft: Int): Seq[Poke] = {
    if (optionsToEvolve.isEmpty) {
      toEvolveSoFar
    } else {
      val topPoke: Poke = optionsToEvolve.head
      val candyCost = PokemonMetaRegistry.getMeta(topPoke.pokemonId).getCandyToEvolve
      if (candyLeft >= candyCost) {
        toEvolve(optionsToEvolve.tail, toEvolveSoFar :+ topPoke, candyLeft - candyCost)
      } else {
        toEvolveSoFar
      }
    }
  }

  private def keepBestByMoveSet(pokemon: Set[Poke]): Set[(Poke, Category)] = {
    val pokesByMoveSet = pokemon.groupBy(_.moveSet)
    val maxPerfectionByMoveSet = pokesByMoveSet.mapValues { pokes => pokes.map(_.perfection).max }
    pokemon.map { poke =>
      if (poke.perfection == maxPerfectionByMoveSet(poke.moveSet)) {
        (poke, Keep)
      } else {
        (poke, Trash)
      }
    }
  }

  private def keepBest(pokemon: Set[Poke]): Set[(Poke, Category)] = {
    if (pokemon.isEmpty) {
      Set.empty
    } else {
      val maxPerfection = pokemon.map(_.perfection).max
      pokemon.map { poke =>
        if (poke.perfection == maxPerfection) {
          (poke, Keep)
        } else {
          (poke, Trash)
        }
      }
    }
  }
}

sealed trait Category
case object Keep extends Category
case object Trash extends Category
case object Evolve extends Category
