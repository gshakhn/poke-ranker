This project connects to the Pokemon GO API and lists all the pokemon you have. It ranks each species by IV so you know which one to keep.

`sbt run` will run it. Follow directions to get your Google authentication code for Pokemon GO.

This program may break Niantic's ToS. Use at your own risk.