name := "poke-ranker"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

resolvers += Resolver.jcenterRepo

libraryDependencies += "com.pokegoapi" % "PokeGOAPI-library" % "0.4.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.0-RC4" % "test"

libraryDependencies += "com.lihaoyi" %% "fansi" % "0.1.3"